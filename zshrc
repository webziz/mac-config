# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
if [[ $OSTYPE == 'darwin'* ]]; then
   export ZSH="/Users/terovepsalainen/.oh-my-zsh"
fi

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"


# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    aws
)

if [[ $OSTYPE == 'darwin'* ]]; then
   source $ZSH/oh-my-zsh.sh
fi

# User configuration

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

if command -v starship &> /dev/null
then
   eval "$(starship init zsh)"
fi

source_if_in_brew(){
   brewhome=$(brew --prefix)   
   #echo $brewhome/$1
   if [[ -f "$brewhome/$1" ]]; then
      source "$brewhome/$1"
   fi
}
# stuff installed with brew
if command -v brew &> /dev/null
then 
   source_if_in_brew "share/zsh-history-substring-search/zsh-history-substring-search.zsh"
   source_if_in_brew "share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
   source_if_in_brew "share/chtf/chtf.sh"
fi

if [[ -f "/Users/terovepsalainen/Work/_azure/bh-azure-terragrunt-infra/_scripts/akslog.sh" ]]; then
    alias akslog='/Users/terovepsalainen/Work/_azure/bh-azure-terragrunt-infra/_scripts/akslog.sh'
fi

if [[ -f "/Users/terovepsalainen/Work/_aws/aws-terragrunt/_scripts/ekslog.sh" ]]; then
    alias ekslog='/Users/terovepsalainen/Work/_aws/aws-terragrunt/_scripts/ekslog.sh'
fi

# setting up BAT
BAT_THEME="Visual Studio Dark+"
export BAT_THEME
alias less="bat --style=plain"
alias cat="bat --style=changes,rule,snip"
alias tgp="terragrunt plan"


# Terraform/Terragrunt things

export TF_PLUGIN_CACHE_DIR=/Users/terovepsalainen/.terraform.d/plugin-cache
scripts_path="/Users/terovepsalainen/Personal/mac-config/scripts"

function chp() {
  cd $($scripts_path/chp.sh $1)
}

function osp() {
  for d in */ ; do
      echo "$d"
      open -a iTerm $d
  done
}

# used by tfswitch
export PATH=$PATH:/Users/terovepsalainen/bin

function git-root() {
   cd $(git rev-parse --show-toplevel)
}

if [[ -d "$scripts_path" ]]; then
   alias chaz="$scripts_path/chaz.sh"
   alias get-azure-env="$scripts_path/getterraformenv.sh /Users/terovepsalainen/Work/_azure/terragrunt"
fi

alias k="kubectl"

TERRAGRUNT_SCRIPT_PATH="/Users/terovepsalainen/Work/bh-toolbox/scripts"

if [[ -d "$TERRAGRUNT_SCRIPT_PATH" ]]; then
   alias akslog="$TERRAGRUNT_SCRIPT_PATH/akslog.sh" # Logs into aks cluster
   alias read_terraform_plan="python3 $TERRAGRUNT_SCRIPT_PATH/read_terraform_plan.py" # Reads all plans in subfolders
   alias refresh_terragrunt="python3 $TERRAGRUNT_SCRIPT_PATH/refresh_terragrunt.py" # Refreshes environments
fi

alias tf='terraform'
alias tfa='terraform apply'
alias tfp='terraform plan'
alias tgp="terragrunt plan"
alias tgpa="terragrunt run-all plan --terragrunt-non-interactive && read_terraform_plan" # Runs plan in all subdirs and provides a handy digest of actions
alias tga="terragrunt apply"

alias tgcc="find . -type d -name '.terragrunt-cache' -prune -exec rm -rf {} \;" # Cleans all cache files in subdirectories
alias tgcl="find . -name \.terraform.lock.hcl -type f -delete" # Cleans all lock files in subdirectories

alias tgpl="python3 $TERRAGRUNT_SCRIPT_PATH/terragrunt_local.py plan" # Runs plan against local modules
alias tgal="python3 $TERRAGRUNT_SCRIPT_PATH/terragrunt_local.py apply" # Runs apply against local modules
alias tgaws="$TERRAGRUNT_SCRIPT_PATH/get_terragrunt_state.sh aws" # Gets terragrunt state information for aws
alias tgazure="$TERRAGRUNT_SCRIPT_PATH/get_terragrunt_state.sh azure" # Gets terragrunt state information for azure

# Created by `pipx` on 2024-08-16 11:06:56
export PATH="$PATH:/Users/terovepsalainen/.local/bin"
