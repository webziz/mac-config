# First run

## Get git
```bash
git
```

## Clone this repo
well, do it

## Install powerline fonts
```
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts 
./install.sh
```
set them to iTerm (for example Fira code is goood)

OR

[Get FiraCode Nerd Font](https://www.nerdfonts.com/font-downloads)

## Install tooling
- [Firefox](https://www.mozilla.org/fi/firefox/new/)
- [Homebrew](https://brew.sh/)
or 
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
- [Oh My Zsh](https://ohmyz.sh/#install)
or
```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

## Install brews
```bash
 brew bundle --file=Brewfile
 ```

## Set the .zshrc file to the repos
```bash
mv ~/.zshrc ~/.zshrc_old
ln -s $PWD/zshrc ~/.zshrc
```

## Set gitconfig
```bash
ln -s $PWD/gitconfig ~/.gitconfig
```

# Sequential runs
## Updating Brewlist
```bash
./update_brew.sh
```

## Update brews
```bash
./install_brew.sh
```