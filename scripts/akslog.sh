#!/bin/bash

## OBS!
## This script _heavily_ assumes that there is only one cluster and will pick the first one

set -e 

if [ $# -ne 2 ]; then 
    echo "akslog - tool to login into aks cluster within an customer env"
    echo "Usage: ./akslog.sh customer environment"
    echo "Example: ./akslog.sh giddy dev"
    exit 0
fi

customer="$1" 
environment="$2"


cluster_info=$(az aks list --subscription "$customer-$environment" |jq '{name: .[0].name, resource_group: .[0].resourceGroup}')

if [ -z "$cluster_info" ]; then
    exit 0
fi

name=$(echo "$cluster_info"| jq '.name' --raw-output)
rg=$(echo "$cluster_info"| jq '.resource_group' --raw-output)

# echo $name
# echo $rg

set -x
az aks get-credentials --admin --name "$name" --resource-group "$rg" --subscription "$customer"-"$environment"


