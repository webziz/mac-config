#!python3

# For terragrunt scrambles the localpath information

# Called in the next manner
# python3 getlocalpath.py PLATFORM_TERRAGRUNT_PATH PLATFORM_MODULES_PATH PWD

import sys
import os
import re

from dotenv import load_dotenv
load_dotenv()

def getModulesPath(module_path, relative_path):
    pass

def main():
    if len(sys.argv) == 2:
        action = sys.argv[1]
    else:
        print("There should be exactly 1 parameter, exiting")
        return False
    
    if action not in ["plan", "apply"]:
        print("The action should be either 'plan' or 'apply'")
        return 50

    # AWS
    aws_modules_path = os.getenv('AWS_MODULES_PATH')
    aws_tg_path = os.getenv('AWS_TERRAGRUNT_PATH')
    azure_modules_path = os.getenv('AZURE_MODULES_PATH')
    azure_tg_path = os.getenv('AZURE_TERRAGRUNT_PATH')

    pwd = os.getcwd()

    if aws_tg_path in pwd:
        # AWS THINGIES
        relative_path = pwd.replace(aws_tg_path, "")
        splitti = relative_path.split('/')
        ends = f"{splitti[-2]}/{splitti[-1]}"

        module_ref_path = f"{aws_modules_path}/{ends}"
        
        if ("/customer/" in module_ref_path):
            module_ref_path = re.sub('\/[a-z]*$', '', module_ref_path)

        if os.path.exists(module_ref_path): 
            bashCommand = f"terragrunt {action} --terragrunt-source {module_ref_path}"
            print(bashCommand)
            os.system(bashCommand)
    elif azure_tg_path in pwd:
        # AZURE THINGIES
        relative_path = pwd.replace(azure_tg_path, "")
        splitti = relative_path.split('/')
        ends = f"{splitti[-2]}/{splitti[-1]}"

        module_ref_path = f"{azure_modules_path}/customer/{ends}"
        print(module_ref_path)
        if os.path.exists(module_ref_path): 
            bashCommand = f"terragrunt {action} --terragrunt-source {module_ref_path}"
            print(bashCommand)
            os.system(bashCommand)
    else:
        print("Could not match the path with AWS nor Azure")
        return 1


if __name__ == "__main__":
    main()