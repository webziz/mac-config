#!/bin/bash
#funky path changer

new_path="."
if [[ $# -ne 0 ]]
then
    target=$1
    IFS='/'     # space is set as delimiter
    read -ra ADDR <<< "$(pwd)"   # str is read into an array as tokens separated by IFS
    for i in "${ADDR[@]}"; do   # access each element of array
        if [ "$i" != "" ]; then
            candidate=$(pwd | sed "s/$i/$target/")
            [ -d "$candidate" ] && new_path=$candidate
        fi
    done
fi
echo "$new_path"
