#!/bin/bash

set -e
PREFIX=$1
CALL_PATH=$(pwd)

if [[ "$CALL_PATH" == "$PREFIX"* ]]
then 
  # echo "call path: $CALL_PATH"
  # echo "prefix: $PREFIX"
  ENDING=${CALL_PATH#$PREFIX}
  CANDIDATE=$(echo "$ENDING"|cut -d"/" -f2)
  if [ ! -z $CANDIDATE ]
  then
    # echo "Candidate: $CANDIDATE"
    # echo "FUTAA"
    az account set --name $CANDIDATE-tenant
    echo "Set az account as '$CANDIDATE-tenant'"
    exit 0
  fi
fi
echo "Could not guess the customer"
exit 1