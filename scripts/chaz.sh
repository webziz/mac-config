#!/bin/sh
# Change Azure account

if [[ $# -eq 0 ]]
then
  az account show
elif [[ $# -eq 1 ]]
then
  az account set --name $1-tenant
else
  echo "give customer name as a parameter, nothing else"
fi
